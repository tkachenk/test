// const express = require('express')
//
// const port = 8080;
// const host = '0.0.0.0';
//
// const app = express();
// app.get('/', (req, res) => {
//   res.send('Hello World');
// });
//
//
//
// app.listen(port, host);
// console.log(`running on http://${host}:${port}`);
const axios = require('axios')

const getOrders = async () => {
  try {
    const res = await axios.get('https://esi.evetech.net/latest/markets/10000002/orders/?datasource=tranquility&order_type=all&page=1')
    if (res.status < 300 && res.data) {
      return res.data
    } else {
      throw 'Ошибка получения данных /markets'
    }
  } catch (e) {
    console.error(e)
  }
}

const sendIds = async (arr) => {
  try {
    const res = await axios.post('https://esi.evetech.net/latest/universe/names/?datasource=tranquility', arr)
    if (res.status < 300 && res.data) {
      return res.data
    } else {
      throw 'Ошибка получения данных /universe'
    }
  } catch (e) {
    console.error(e)
  }
}

const getResult = async () => {
  const arrOrders = await getOrders()
  if (!arrOrders || !arrOrders.length) return 'Отсутствуют данные /markets'
  
  const ids = arrOrders.map(v => v.type_id)
  const uniqIds = new Set(ids)   
  const arrUniverse = await sendIds(Array.from(uniqIds))
  if (!arrUniverse || !arrUniverse.length) return 'Отсутствуют данные /universe'
  
  const universeNames = arrUniverse.map(v => v.name)
  const sortedNames = universeNames.sort()
  return sortedNames.join('\n')
}

getResult().then(result => {
  console.log(result)
})
