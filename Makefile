FILENAME=tkeded/test

all: build run

build:
	docker build . -t $(FILENAME)

run:
	docker run --rm $(FILENAME)

clear:
	docker rmi $(FILENAME)
